/**
 * Cau so 1
 * Tao Mang 100 phan tu
 * 10 ngang 10 doc
 * hang la lien tiep vd : 0 1  2  3 .. 9
 * cot la hon kem 10 vd  10 11 12 13 .. 19
 */
function cauSoMot() {
  let htmlStr = "";
  for (let index = 0; index < 100; index++) {
    if (index % 2 == 0) {
      htmlStr += `<div class="col bg-danger">${index}</div>`;
    } else {
      htmlStr += `<div class="col bg-primary">${index}</div>`;
    }
  }
  // generate html
  const containerNode = document.querySelector("#container");
  if (containerNode) {
    containerNode.innerHTML = htmlStr;
  }

  //   document.querySelector("#container").innerHTML = htmlStr;
}

/**
 * Cau so 2:
 * Viet function nhan vao 1 mang
 * Tim va in ra cac so nguyen to
 */
let arr2 = [];
function themSoVaoMang() {
  const inputNode = document.querySelector("#text-2");
  if (inputNode.value * 1 >= 0) {
    arr2.push(inputNode.value * 1);
  }
  console.log(arr2);
}
function cauSoHai() {
  // Tim cac so nguyen to ( chi chia het cho 1 va chinh no)
  const rs = arr2.filter((number) => {
    let filterResult = true;
    if (number < 2) {
      return false;
    }
    if (number === 2) {
      return true;
    }
    if (number > 2) {
      for (let i = 2; i <= Math.sqrt(number); i++) {
        if (number % i == 0) {
          console.log({ number, i });
          filterResult = false;
          break;
        }
      }
    }
    console.log({ number, filterResult });
    return filterResult;
  });
  const resultNode = document.querySelector("#kq-cau-2");
  const mangDaNhap = `Mang da nhap: ${arr2.toString()}<br>`;
  if (resultNode) {
    if (rs.length) {
      resultNode.innerHTML =
        mangDaNhap + `Cac so nguyen to trong mang: ${rs.toString()}`;
    } else {
      resultNode.innerHTML = mangDaNhap + `Khong tim thay so nguyen to`;
    }
  }
}

// cau 3: nhap n, tinh S= (2+3+...+n)+2n
function themSo() {
  const number = document.querySelector("#text-3").value * 1;
  console.log("number: ", number);

  if (number >= 3) {
    let Tong = 2 * number;

    for (let i = 2; i < number; i++) {
      Tong = Tong + i;
      document.getElementById("kq-cau-3").innerHTML = Tong;
    }
  } else {
    document.getElementById("kq-cau-3").innerText = "Nhap so lon hon 2";
  }
}

// cau 4 tim uoc cua so n lon hon 0
function timUoc() {
  const number = document.querySelector("#text-4").value * 1;
  console.log("number: ", number);
  if (number > 0) {
    arr4 = [];
    for (i = 1; i <= number; i++) {
      if (number % i === 0) {
        arr4.push(i);
      }
      document.getElementById("kq-cau-4").innerText = arr4.toString();
    }
  } else {
    document.getElementById("kq-cau-4").innerText = "Nhap so lon hon 0";
  }
}

//cau 5 dao nguoc so

function daoNguocSo() {
  const str = document.querySelector("#text-5").value;
  if (str * 1 > 0) {
    const arr = str.split(""); // 1234 => [1,2,3,4]
    let result = "";
    if (arr.length) {
      for (
        var i = arr.length - 1 /** neu mang = [1,2,3,4] thi i = 3 */;
        i >= 0;
        i--
      ) {
        result += arr[i];
      }
    }
    document.getElementById("kq-cau-5").innerText = result;
  } else {
    document.getElementById("kq-cau-5").innerText = "Nhap so lon hon 0";
  }
}

// cau 6 tim x : 1+2+3+..+x <= 100

function cau6() {
  let result = 0;
  let s = 0;
  for (let index = 0; index <= 100; index++) {
    s += index;
    if (s > 100) {
      result = index - 1;
      break;
    }
  }
  document.getElementById("kq-cau-6").innerText = result;
}

// cau 7
function cau7() {
  const number = document.getElementById("cau7").value * 1;
  let rs = "";
  for (let index = 0; index < 11; index++) {
    rs += `${number} x ${index} = ${index * number}<br>`; // vd: number = 3; index = 0 => rs  = "" + "3 x 0 = 3*0 <br>"
  }
  document.getElementById("ketquacau7").innerHTML = rs;
}

//cau 8
function cau8() {
  var cards = [
      "4K", //0
      "KH", // 1
      "5C", // 2
      "KA", // 3
      "QH", // 4
      "KD", // 5
      "2H", // 6
      "10S", // 7
      "AS", //8
      "7H", //9
      "9K", // 10
      "10D", // 11
    ],
    player = [[], [], [], []];

  for (let index = 0; index < cards.length; index += player.length) {
    for (var i = 0; i < player.length; i++) {
      player[i].push(cards[index + i]);
    }
  }
  /**
   * index = 0
   * i = 0;
   * player[0].push(cards[0 + 0])
   * player[1].push(cards[0 + 1])
   * player[2].push(cards[0 + 2])
   * index = 4
   * i = 0;
   * player[0].push(cards[4 + 0])
   * player[1].push(cards[4 + 1])
   * player[2].push(cards[4 + 2])
   * player[3].push(cards[4 + 3])
   */

  let rs = "";
  player.map((player) => (rs += player.toString() + "<br>"));
  document.getElementById("ketquacau8").innerHTML = rs;
}

//cau 9 cau 10 khong biet lam mentor oi
